import React, { Component } from 'react';
import { connect } from 'react-redux';


class EditComponent extends Component {
  handleEdit = (e) => {
    e.preventDefault();
    const newTitle = this.getTitle.value;
    const newMessage = this.getMessage.value;
    const newExample = this.getExample.value;
    const data = {
      newTitle,
      newMessage,
      newExample
    }
    console.log('editcomponent')
    console.log(data)
    this.props.dispatch({ type: 'UPDATE', id: this.props.post.id, data: data })
  }
  render() {
    return (
      <div key={this.props.post.id} className="post">
        <form className="form" onSubmit={this.handleEdit}>
          <input required type="text" ref={(input) => this.getTitle = input}
            defaultValue={this.props.post.title} placeholder="Enter Post Title" /><br /><br />
          <textarea required rows="5" ref={(input) => this.getMessage = input}
            defaultValue={this.props.post.message} cols="28" placeholder="Enter Post" /><br /><br />
          <input required type="text" ref={(input) => this.getExample = input}
            defaultValue={this.props.post.example} placeholder="Enter Post Example" />
          <button>Update</button>
        </form>
      </div>
    );
  }
}
export default connect()(EditComponent);
